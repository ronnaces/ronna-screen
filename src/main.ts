import {createApp} from "vue";
import "./style/main.less";
import App from "./App.vue";

const app = createApp(App);

app.mount("#app");
