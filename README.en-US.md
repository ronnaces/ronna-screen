<h1>ronna-screen</h1>

![doc.png](public%2Fdoc.png)

![GitHub license](https://img.shields.io/github/license/ronnaces/ronna-screen?style=flat)
![GitHub stars](https://img.shields.io/github/stars/ronnaces/ronna-screen?color=fa6470&style=flat)
![GitHub forks](https://img.shields.io/github/forks/ronnaces/ronna-screen?style=flat)

**English** | [中文](./README.md)

****

## Introduction

`ronna-screen` is an open source, free and out-of-the-box middle and backend management system template. Completely
adopts `ECMAScript` module (`ESM`) specifications to write and organize code, using the
latest `Vue3`, `Vite`, `Element-Plus`, `TypeScript`, `Pinia`, `Tailwindcss` and other mainstream technologies develop

## Docs

[View ronna-screen documentation](https://ronnaces.github.io/ronna-screen-doc/)

## Quality service, software outsourcing, sponsorship support

[Click me for details](https://ronnaces.github.io/ronna-screen-doc/pages/service/)

## Preview

[preview station](https://screen.ronnaces.com/)

### Use Gitpod

Open the project in Gitpod (free online dev environment for GitHub) and start coding immediately.

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://github.com/ronna-screen/ronna-screen)

## Install And Use

### Pull code

#### Pull from `GitHub`

```bash
git clone https://github.com/ronnaces/ronna-screen.git
```

### Install dependencies

```bash
cd ronna-screen

pnpm install
```

### Run platform

```bash
pnpm dev
```

### Project packaging

```bash
pnpm build
```

## Docker support

1. Customize the image named `ronna-screen` (please note that there is a dot `.` at the end of the command below,
   indicating that the `Dockerfile` file in the current path is used, and the path can be specified according to the
   actual situation)

```bash
docker build -t ronna-screen .
```

2. Port mapping and start the `docker` container (`8080:80`: indicates that the `80` port is used in the container, and
   the port is forwarded to the `8080` port of the host; `ronna-screen`: indicates a custom container
   name; `ronna-screen`: indicates the custom image name)

```bash
docker run -dp 8080:80  --name ronna-screen ronna-screen
```

After operating the above two commands, open `http://localhost:8080` in the browser to preview

Of course, you can also operate the `docker` project through
the [Docker Desktop](https://www.docker.com/products/docker-desktop/) visual interface, as shown below

## Change Log

[CHANGELOG](./CHANGELOG.en_US.md)

## How to contribute

You are very welcome to join！[Raise an issue](https://github.com/ronnaces/ronna-screen/issues/new/choose) Or submit a
Pull Request

**Pull Request:**

1. Fork code!
2. Create your own branch: `git checkout -b feat/xxxx`
3. Submit your changes: `git commit -am 'feat(function): add xxxxx'`
4. Push your branch: `git push origin feat/xxxx`
5. submit`pull request`

## Git Contribution submission specification

reference [vue](https://github.com/vuejs/vue/blob/dev/.github/COMMIT_CONVENTION.md)
specification ([Angular](https://github.com/conventional-changelog/conventional-changelog/tree/master/packages/conventional-changelog-angular))

- `feat` Add new features
- `fix` Fix the problem/BUG
- `style` The code style is related and does not affect the running result
- `perf` Optimization/performance improvement
- `refactor` Refactor
- `revert` Undo edit
- `test` Test related
- `docs` Documentation/notes
- `chore` Dependency update/scaffolding configuration modification etc.
- `workflow` Workflow improvements
- `ci` Continuous integration
- `types` Type definition file changes
- `wip` In development

## Browser support

It is recommended to use `Chrome`, `Edge`, and `Firefox` browsers for local development. The author commonly uses the
latest version of `Chrome` browser.  
In actual use, I feel that `Firefox` is smoother in animation than other browsers, but the author is used to
using `Chrome`. It depends on personal preference.  
For more detailed browser compatibility support, please
see [Which browsers does Vue support? ](https://vuejs.org/about/faq.html#what-browsers-does-vue-support)
and [Vite browser compatibility](https://vitejs.dev/guide/build.html#browser-compatibility)

| [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_48x48.png" alt=" Edge" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>IE | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_48x48.png" alt=" Edge" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Edge | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/firefox/firefox_48x48.png" alt="Firefox" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Firefox | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/chrome/chrome_48x48.png" alt="Chrome" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Chrome | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/safari/safari_48x48.png" alt="Safari" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Safari |
|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
|                                                                                             not support                                                                                              |                                                                                            last 2 versions                                                                                             |                                                                                                  last 2 versions                                                                                                  |                                                                                                last 2 versions                                                                                                |                                                                                                last 2 versions                                                                                                |

## Maintainer

[kunlong-luo](https://github.com/kunlong-luo)

## License

Completely free and open source

[MIT © 2024-present, ronna-screen](./LICENSE)
