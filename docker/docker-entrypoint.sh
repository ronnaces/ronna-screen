#!/bin/bash
################################################
# docker-entrypoint
# @author KunLong-Luo
# @version 1.0.0
# @since 2023-03-16 10:50:00
################################################

cp -rfv /ronna/dist/* /usr/share/nginx/html/
cp -rfv /ronna/*.conf /etc/nginx/conf.d/
nginx -g "daemon off;"
